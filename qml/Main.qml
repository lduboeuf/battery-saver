/*
 * Copyright (C) 2023  Your FullName
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * batterysaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import Qt.labs.platform 1.0
import Lomiri.Components.Popups 1.3
import MeeGo.QOfono 0.2


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'batterysaver.lduboeuf'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property var modems: []
    property var connectivityNetworks: []
    property string modem: ""
    property string defaultNetwork: "gsm"
    property string preferredNetwork: "lte"
    property bool disableWifi: false
    property bool disableCellularDataWifiOn: false
    property bool mobileSpeedDependsOnLockState: false
    property bool running: false
    property bool checking: true
    readonly property var technologies: {
        'gsm': "2G only (gsm)",
        'umts': "2G/3G (umts)",
        'lte': "2G/3G/4G (lte)"
    }
    property var techModel: []

    signal configChanged()

    function setConfig() {
        //root.modem = root.modems[0]
        root.defaultNetwork = root.connectivityNetworks[defaultConnectivity.selectedIndex]
        root.preferredNetwork = root.connectivityNetworks[preferredConnectivity.selectedIndex]
        console.log('config set to:', root.modem, root.defaultNetwork, root.preferredNetwork, root.disableCellularDataWifiOn)
    }

    function start() {
        root.setConfig()

        python.call('main.restart', [], function(returnValue) {
            if (returnValue !== undefined) root.running = returnValue
            console.log('start returned ' + returnValue);
        })
    }

    function stop() {
        python.call('main.stop', [], function(returnValue) {
            if (returnValue !== undefined) root.running = returnValue
            console.log('stop returned ' + returnValue);
        })
    }

    onConfigChanged: {
        if (running) {
            root.stop()
        }
    }

    onConnectivityNetworksChanged: {
        var model = []
        root.connectivityNetworks.forEach( n => model.push(root.technologies[n]))
        root.techModel = model
        console.log('model:', model)
    }

    Settings {
        id: settings
        property alias modem: root.modem
        property alias defaultNetwork: root.defaultNetwork
        property alias preferredNetwork: root.preferredNetwork
        property alias disableWifi: root.disableWifi
        property alias disableCellularDataWifiOn: root.disableCellularDataWifiOn
        property alias mobileSpeedDependsOnLockState: root.mobileSpeedDependsOnLockState
    }

    Component {
         id: dialog
         Dialog {
             id: dialogue
             title: i18n.tr("Battery Saver")
             text: i18n.tr("This app allows to switch to preferred network when cellular data is on, and toggle to default when cellular data is off, app can be closed if running state is shown")
             Button {
                 text: "ok"
                 onClicked: PopupUtils.close(dialogue)
             }
         }
    }

    Page {
        id: emptyPage
        anchors.fill: parent
        visible: root.modem.length === 0 || root.techModel.length === 0
        header: PageHeader {
            title: i18n.tr('BatterySaver')
        }

        Sections {
            visible: root.modems.length > 1
            anchors.horizontalCenter: parent.horizontalCenter
            model: ["SIM 1", "SIM 2"]
            onSelectedIndexChanged: {
                root.modem = root.modems[selectedIndex]
            }
        }

        Label {
            id: noSimCardLbl
            anchors.centerIn: parent
            text: i18n.tr('No SIM card detected!')
        }
    }

    Page {
        id: page
        anchors.fill: parent
        visible: !emptyPage.visible

        header: PageHeader {
            id: header
            title: i18n.tr('BatterySaver')
            subtitle: root.checking ? i18n.tr('Checking...') : root.running ? i18n.tr('Running...') : i18n.tr('Stopped')
            Row {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                spacing: units.gu(1)

                Icon {
                    id: helpIcon
                    anchors.verticalCenter: parent.verticalCenter
                    width: units.gu(2)
                    height: units.gu(2)
                    name: "help"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: PopupUtils.open(dialog)
                    }
                }

                Switch {
                    id: customDelegateBar
                    enabled: root.modems.length > 0 && !root.checking
                    checked: root.running
                    onClicked: {
                        if (root.running) {
                            root.stop()
                        } else {
                            root.start()
                        }
                    }
                }
            }

        }

        Column {
            id: prefs
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: units.gu(4)
            anchors.margins: units.gu(2)
            spacing: units.gu(1)

            SectionTitle {
                title: i18n.tr("When cellular data is off:")
            }

            OptionSelector {
                id: defaultConnectivity
                text: i18n.tr("preferred network:")
                model: root.techModel

                onDelegateClicked: {
                    selectedIndex = index
                    root.configChanged()
                }
            }

            SectionTitle {
                title: i18n.tr("When cellular data is on:")
            }

            OptionSelector {
                id: preferredConnectivity
                text: i18n.tr("preferred network:")
                model: root.techModel
                onDelegateClicked: {
                    selectedIndex = index
                    root.configChanged()
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("only select when screen is unlocked")
                    Switch {
                        id: switchReduceOnLockItem
                        SlotsLayout.position: SlotsLayout.Trailing
                        checked: root.mobileSpeedDependsOnLockState
                        onCheckedChanged: {
                            if (checked != root.mobileSpeedDependsOnLockState) {
                                root.mobileSpeedDependsOnLockState = checked
                                root.configChanged()
                            }
                        }
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("toggle off wifi")
                    Switch {
                        id: switchToggleWifiItem
                        SlotsLayout.position: SlotsLayout.Trailing
                        checked: root.disableWifi
                        onCheckedChanged: {
                            root.disableWifi = checked
                            root.configChanged()
                        }
                    }
                }
            }

            SectionTitle {
                title: i18n.tr("When wifi is on:")
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("toggle off cellular data")
                    Switch {
                        SlotsLayout.position: SlotsLayout.Trailing
                        checked: root.disableCellularDataWifiOn
                        onCheckedChanged: {
                            root.disableCellularDataWifiOn = checked
                            root.configChanged()
                        }
                    }
                }
            }

            Label {
                id: errorLabel
                width: parent.width
                color: "red"
                wrapMode: Label.WordWrap
            }
        }
    }

    OfonoManager {
        id: ofonoManager
        onModemsChanged: {
            root.modems = modems.slice(0).sort()
            if (root.modems.length > 0 && root.modem.length === 0) {
                root.modem = root.modems[0]
            }

            console.log("ofonoManager",root.modems )
            console.log("modem1: " + root.modems[0] )
            console.log("modem2: " + root.modems[1] )
        }
    }


    OfonoRadioSettings {
        id: radioSettings
        modemPath: root.modem
        onAvailableTechnologiesChanged: {
            root.connectivityNetworks = technologies
            console.log('techs:',  root.connectivityNetworks)
            // need to be defined here, otherwise if within OptionSelector , it switch from correct index, to 0
            defaultConnectivity.selectedIndex = root.connectivityNetworks.indexOf(root.defaultNetwork)
            preferredConnectivity.selectedIndex = root.connectivityNetworks.indexOf(root.preferredNetwork)
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));
            importModule('main', function() {
                console.log('module main imported');
                python.call('main.init', [], function(returnValue) {
                    root.checking = false
                    if (returnValue !== undefined) root.running = returnValue
                    console.log('init returned ' + returnValue);
                })
            });
        }

        onError: {
            console.log('python error: ' + traceback);
            errorLabel.text = "oups there is an error, please check the log"
        }
    }

}
